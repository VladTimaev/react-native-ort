import { StyleSheet, View} from 'react-native';
import Search from './src/components/Search';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HomeStackScreen from './src/Router';
import Footer from './src/Router';
import SettingsScreen from './src/components/SettingsScreen';
import icon from './assets/favicon.png';

const Tab = createBottomTabNavigator();
export default function App() {
  return (
  <NavigationContainer>
    <Tab.Navigator screenOptions ={({ route }) => ({
      tabBarIcon: ({ focused, color, size }) => {
        let iconName;
        if (route.name === 'Home') {
          iconName = focused
          ? 'ios-information-circle' : 'ios-information-circle-outline';
        } else if (route.name === 'Settings') {
          iconName = focused ? 'ios-list-box' : 'ios-list';
        }
        return <Ionicons name={"navigate-circle-outline"} size={20} color={color} />;
      },
      tabBarActiveTintColor: 'tomato',
      tabBarInactiveTintColor: 'gray',
    })}>
      <Tab.Screen options={{headerShown: false}} name="Главная" component={HomeStackScreen}  />
      <Tab.Screen options={{headerShown: false}} name="Поиск" component={Search} />
      <Tab.Screen options={{headerShown: false}} name="Информация" component={SettingsScreen} />
    </Tab.Navigator>
  </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    backgroundColor: "#fff"
  },
  text: {
    color: "#fff"
  }
});
