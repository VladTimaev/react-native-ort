import React from 'react';
import { StyleSheet, Text, View, Button, ScrollView} from 'react-native';
import Home from './Home';
import Img from './Img';

export default function HomeScreen ({ navigation }) {
    return (
     <ScrollView>
       <View>
        <Home />
        <Button title="Выбрать предмет" onPress={() => navigation.navigate('Предметы')} />
       </View>
      </ScrollView>
    );
  };
