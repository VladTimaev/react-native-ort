import React from "react";
import {View, StyleSheet, Button, Text} from "react-native";


const date = new Date(0);
const correctMinutes = date.getMinutes();
const correctSeconds = date.getSeconds();
const correctHours = date.getHours();

function addZero(i) {
  if (i < 10) {i = "0" + i}
  return i;
}

let h = addZero(correctHours);
let m = addZero(correctMinutes);
let s = addZero(correctSeconds);
let time = h + ":" + m + ":" + s;

console.log(time)

export default function Test ({ navigation }) {
  return (
  <View style={style.container}>
    <View style={style.row}>
      <Text style={style.title}>Оставшееся время: {time}</Text>
     <Button style={style.button}
     title="Закончить тест"
     onPress={() => navigation.navigate('Главная')} />
    </View>
  </View>
  )
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
},
 row: {
  flexDirection: "row",
  justifyContent: "space-between"
 },
 title: {
  color: "#93ABD3"
 }
});
