import React, { useState } from "react";
import DATA from "../data/data";
import { FlatList, SafeAreaView, StyleSheet, Text, TouchableOpacity, View, ScrollView } from "react-native";

const Item = ({ item, backgroundColor, textColor}) => (
  <TouchableOpacity  onPress={() => {navigate('Тест')}} style={[styles.item, backgroundColor]}>
    <Text style={[styles.title, textColor]}>{item.title}</Text>
    <View style={styles.titleBottom}>
         <Text>{item.time}</Text>
         <Text>{item.task}</Text>
      </View>
  </TouchableOpacity>
);


const Card = () => {
  const [selectedId, setSelectedId] = useState(null);
  const renderItem = ({ item }) => {
    const backgroundColor = item.id === selectedId ? item.color : item.color;
    const color = item.id === selectedId ? 'white' : 'black';

    return (
      <Item
        item={item}
        onPress={() => setSelectedId(item.id)}
        backgroundColor={{ backgroundColor }}
        textColor={{ color }}
      />

    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={DATA}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        extraData={selectedId}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: "center"
  },
  item: {
    width: 340,
    height: 210,
    borderRadius: 30,
    padding: 25,
    margin: 10,
    cursor: "pointer"
  },
  title: {
    fontFamily: 'Roboto',
    fontSize: 26,
    color: "#000"
  },
  titleBottom: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 100
  }
});

export default Card;
