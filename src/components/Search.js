import React from 'react';
import { StyleSheet, Text, View, Button, TextInput, ScrollView} from 'react-native';
import { Animated } from 'react-native';
import {useCollapsibleSubHeader, CollapsibleSubHeaderAnimator} from 'react-navigation-collapsible';
import Card from "./Card";


export default function Search ({ navigation}) {

    return (
      <View style={{ flex: 1}}>
        <ScrollView>
          <MyScreen />
        </ScrollView>
        <Button title="Назад" onPress={() => navigation.goBack()} />
        <Button title="Go to Details" onPress={() => navigation.navigate('Поиск')} />
      </View>
    );
  };

const MyScreen = ({ navigation, route }) => {

  const {
    onScroll /* Event handler */,
    containerPaddingTop /* number */,
    scrollIndicatorInsetTop /* number */,
    translateY,
  } = useCollapsibleSubHeader();


  const MySearchBar = () => (
    <View style={{ padding: 15, width: '100%', height: 60 }}>
      <TextInput placeholder="Поиск" />
    </View>
  );
  return (
    <>
      <Animated.FlatList
        onScroll={onScroll}
        contentContainerStyle={{ paddingTop: containerPaddingTop }}
        scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
      />
      <CollapsibleSubHeaderAnimator translateY={translateY}>
       <MySearchBar />
       <Card />
      </CollapsibleSubHeaderAnimator>
    </>
  );
};
