import React from 'react';
import { SafeAreaView, View, VirtualizedList, StyleSheet, Text, StatusBar, ScrollView } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';


const DATA = [];
const getItem = (data, index) => ({
  id: Math.random().toString(12).substring(0),
  title: `Item ${index+1}`
});
const getItemCount = (data) => 3;


const Item = ({ title }) => (
<TouchableOpacity>
  <View style={styles.item}>
     <Text style={styles.titles}>{title}</Text>
     <Text style={styles.title}>{title}</Text>
    </View>
  </TouchableOpacity>
);

const Home = () => {
  return (
    <SafeAreaView style={styles.container}>
      <VirtualizedList
        data={DATA}
        initialNumToRender={4}
        renderItem={({ item }) => <Item title={item.title} />}
        keyExtractor={item => item.key}
        getItemCount={getItemCount}
        getItem={getItem}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight,
  },
  item: {
    height: 120,
    marginVertical: 0,
    marginHorizontal: 0,
    padding: 15,
  },
  titles: {
   color: "#fff",
   fontSize: 16,
   margin: 0,
   padding: 5,
   lineHeight: 30,
   backgroundColor: '#3AB0FF',
   borderRadius: 5
  },
  title: {
    fontSize: 20,
    backgroundColor: "#fff",
    padding: 5,
    height: 100,
    borderRadius: 5
  },
});

export default Home;
