import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import List from './List';

export default function SettingsScreen({ navigation }) {
    return (
      <View style={{ flex: 1}}>
        <Text>Settings screen</Text>
        <List />
        <Button title="Назад" onPress={() => navigation.goBack()} />
      </View>
    );
  };
