import React from 'react';
import { StyleSheet, Text, View, Button} from 'react-native';

export default function DetailsScreen ({ navigation }) {

    return (
     <View style={style.container}>
       <View style={style.row}>
        <Button title="Назад" onPress={() => navigation.goBack()} />
        <Button title="Предметы" onPress={() => navigation.navigate('Предметы')} />
        <Button title="Начать тест" onPress={() => navigation.navigate('Тест')} />
       </View>
      </View>
    );
  };



const style = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center"
    },
    row: {
       flexDirection: "row",
       justifyContent: "space-between"
    }
});
