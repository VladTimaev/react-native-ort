import React from "react";
import { ActivityIndicator, StyleSheet, Text, View } from "react-native";


// Установить во время загрузки приложения Bag#2 loader.json

const Loader = () => (
  <View style={[styles.container, styles.horizontal]}>
    <ActivityIndicator size="large" color="#3AB0FF" />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});

export default Loader;
