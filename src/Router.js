import React from "react";
import {View} from "react-native";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from "./components/HomeScreen";
import DetailsScreen from "./components/DetailsScreen";
import Test from "./components/Test";
import Card from "./components/Card";
import SettingsScreen from "./components/SettingsScreen";

export default function HomeStackScreen () {

  const HomeStack = createNativeStackNavigator();
  return (
  <HomeStack.Navigator>
    <HomeStack.Screen name="Подготовка к ОРТ" component={HomeScreen} />
    <HomeStack.Screen name="Details" component={DetailsScreen} />
    <HomeStack.Screen name="Тест" component={Test} />
    <HomeStack.Screen name="Предметы" component={Card} />
  </HomeStack.Navigator>
  )
};



export function Footer () {
    const SettingsStack = createNativeStackNavigator();
    return (
     <SettingsStack.Navigator>
      <SettingsStack.Screen name="Информация" component={SettingsScreen} />
      <SettingsStack.Screen name="Details" component={DetailsScreen} />
     </SettingsStack.Navigator>
    )
  };
